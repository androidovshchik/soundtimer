package rf.androidovshchik.soundtimer;

import android.app.Application;

import com.facebook.stetho.Stetho;

import timber.log.Timber;

public class SoundTimer extends Application {

	@Override
	public void onCreate() {
		super.onCreate();
		Timber.plant(new Timber.DebugTree());
		if (BuildConfig.DEBUG) {
			Stetho.initializeWithDefaults(getApplicationContext());
		}
	}
}