package rf.androidovshchik.soundtimer.models.rows;

import android.content.ContentValues;
import android.database.Cursor;

public class Metadata extends Row {

	private static final String COLUMN_ID = "rowid";
	private static final String COLUMN_NAME = "name";
	private static final String COLUMN_TITLE = "title";

	public String name;

	public String title;

	@Override
	public ContentValues toContentValues() {
		ContentValues values = new ContentValues();
		if (id != NONE) {
			values.put(COLUMN_ID, id);
		}
		values.put(COLUMN_NAME, name);
		values.put(COLUMN_TITLE, title);
		return values;
	}

	@Override
	public void parseCursor(Cursor cursor) {
		id = cursor.getLong(cursor.getColumnIndexOrThrow(COLUMN_ID));
		name = cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_NAME));
		title = cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_TITLE));
	}

	@Override
	public String toString() {
		return "Metadata{" +
				"table='" + table + '\'' +
				", id=" + id +
				", name='" + name + '\'' +
				", title='" + title + '\'' +
				'}';
	}
}
