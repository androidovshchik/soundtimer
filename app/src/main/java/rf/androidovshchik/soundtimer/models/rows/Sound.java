package rf.androidovshchik.soundtimer.models.rows;

import android.content.ContentValues;
import android.database.Cursor;

public class Sound extends Row {

	private static final String COLUMN_ID = "rowid";
	private static final String COLUMN_DELAY = "delay";
	private static final String COLUMN_MESSAGE = "message";
	private static final String COLUMN_SIGNAL = "signal";
	private static final String COLUMN_FILE = "file";

	public int delay;

	public String message;

	public String signal;

	public String file;

	@Override
	public ContentValues toContentValues() {
		ContentValues values = new ContentValues();
		if (id != NONE) {
			values.put(COLUMN_ID, id);
		}
		values.put(COLUMN_DELAY, delay);
		values.put(COLUMN_MESSAGE, message);
		values.put(COLUMN_SIGNAL, signal);
		values.put(COLUMN_FILE, file);
		return values;
	}

	@Override
	public void parseCursor(Cursor cursor) {
		id = cursor.getLong(cursor.getColumnIndexOrThrow(COLUMN_ID));
		delay = cursor.getInt(cursor.getColumnIndexOrThrow(COLUMN_DELAY));
		message = cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_MESSAGE));
		signal = cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_SIGNAL));
		file = cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_FILE));
	}

	@Override
	public String toString() {
		return "Sound{" +
				"table='" + table + '\'' +
				", id=" + id +
				", delay=" + delay +
				", message='" + message + '\'' +
				", signal='" + signal + '\'' +
				", file='" + file + '\'' +
				'}';
	}
}
