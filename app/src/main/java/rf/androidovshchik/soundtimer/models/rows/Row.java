package rf.androidovshchik.soundtimer.models.rows;

import android.content.ContentValues;
import android.database.Cursor;
import android.support.annotation.NonNull;

import java.util.ArrayList;

public abstract class Row {

	// id
	public static final long NONE = 0L;

	@NonNull
	public String table;

	public long id = NONE;

	public abstract ContentValues toContentValues();

	public abstract void parseCursor(Cursor cursor);

	public static <T extends Row> ArrayList<T> getRows(Cursor cursor, Class<T> rowClass) throws Exception {
		ArrayList<T> rows = new ArrayList<>();
		if (cursor == null) {
			return rows;
		}
		try {
			while (cursor.moveToNext()) {
				T row = rowClass.newInstance();
				row.parseCursor(cursor);
				rows.add(row);
			}
		} finally {
			cursor.close();
		}
		return rows;
	}
}
