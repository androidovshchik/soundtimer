package rf.androidovshchik.soundtimer.data;

import android.arch.persistence.db.SupportSQLiteOpenHelper;
import android.arch.persistence.db.SupportSQLiteOpenHelper.Configuration;
import android.arch.persistence.db.SupportSQLiteOpenHelper.Factory;
import android.arch.persistence.db.framework.FrameworkSQLiteOpenHelperFactory;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.squareup.sqlbrite3.BriteDatabase;
import com.squareup.sqlbrite3.SqlBrite;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.schedulers.Schedulers;
import rf.androidovshchik.soundtimer.BuildConfig;
import rf.androidovshchik.soundtimer.models.rows.Row;

public class DbManager {

    public BriteDatabase db;

    public DbManager() {}

    public void openDb(Context context, String path, boolean delete) {
        closeDb();
        DbCallback dbCallback = new DbCallback();
        dbCallback.openDatabase(context, path, delete);
        Configuration configuration = Configuration.builder(context)
                .name(DbCallback.DATABASE_NAME)
                .callback(dbCallback)
                .build();
        Factory factory = new FrameworkSQLiteOpenHelperFactory();
        SupportSQLiteOpenHelper openHelper = factory.create(configuration);
        db = new SqlBrite.Builder()
                .logger(message -> Log.v(getClass().getSimpleName(), message))
                .build()
                .wrapDatabaseHelper(openHelper, Schedulers.io());
        db.setLoggingEnabled(BuildConfig.DEBUG);
    }

    private void closeDb() {
        if (db != null) {
            db.close();
            db = null;
        }
    }

    @SuppressWarnings("all")
    public Observable<Boolean> onExecSql(String sql) {
        return Observable.create((ObservableEmitter<Boolean> emitter) -> {
            if (emitter.isDisposed()) {
                return;
            }
            BriteDatabase.Transaction transaction = db.newTransaction();
            try {
                db.execute(sql);
                transaction.markSuccessful();
            } finally {
                transaction.end();
            }
            emitter.onNext(true);
            emitter.onComplete();
        }).subscribeOn(Schedulers.io());
    }

    @SuppressWarnings("all")
    public Observable<Cursor> onSelectTable(String sql) {
        return Observable.create((ObservableEmitter<Cursor> emitter) -> {
            if (emitter.isDisposed()) {
                return;
            }
            Cursor cursor = null;
            BriteDatabase.Transaction transaction = db.newTransaction();
            try {
                cursor = db.query(sql);
                transaction.markSuccessful();
            } finally {
                transaction.end();
            }
            if (cursor != null) {
                emitter.onNext(cursor);
            }
            emitter.onComplete();
        }).subscribeOn(Schedulers.io());
    }

    @SuppressWarnings("all")
    public Observable<Row> onInsertRow(Row row) {
        return Observable.create((ObservableEmitter<Row> emitter) -> {
            if (emitter.isDisposed()) {
                return;
            }
            BriteDatabase.Transaction transaction = db.newTransaction();
            try {
                row.id = insertRow(row);
                transaction.markSuccessful();
            } finally {
                transaction.end();
            }
            emitter.onNext(row);
            emitter.onComplete();
        }).subscribeOn(Schedulers.io());
    }

    @SuppressWarnings("all")
    public long insertRow(Row row) {
        return db.insert(row.table, SQLiteDatabase.CONFLICT_REPLACE, row.toContentValues());
    }

    @SuppressWarnings("all")
    public Observable<Integer> onUpdateRow(Row row) {
        return Observable.create((ObservableEmitter<Integer> emitter) -> {
            if (emitter.isDisposed()) {
                return;
            }
            int result = 0;
            BriteDatabase.Transaction transaction = db.newTransaction();
            try {
                result = updateRow(row);
                transaction.markSuccessful();
            } finally {
                transaction.end();
            }
            emitter.onNext(result);
            emitter.onComplete();
        }).subscribeOn(Schedulers.io());
    }

    @SuppressWarnings("all")
    public int updateRow(Row row) {
        return db.update(row.table, SQLiteDatabase.CONFLICT_IGNORE, row.toContentValues(),
            "rowid=?", String.valueOf(row.id));
    }

    @SuppressWarnings("all")
    public Observable<Integer> onDeleteRow(Row row) {
        return Observable.create((ObservableEmitter<Integer> emitter) -> {
            if (emitter.isDisposed()) {
                return;
            }
            int result = 0;
            BriteDatabase.Transaction transaction = db.newTransaction();
            try {
                result = deleteRow(row);
                transaction.markSuccessful();
            } finally {
                transaction.end();
            }
            emitter.onNext(result);
            emitter.onComplete();
        }).subscribeOn(Schedulers.io());
    }

    @SuppressWarnings("all")
    public int deleteRow(Row row) {
        return db.delete(row.table, "rowid=?", String.valueOf(row.id));
    }

    @SuppressWarnings("all")
    public int deleteTable(String table) {
        return db.delete(table, null, null);
    }
}
