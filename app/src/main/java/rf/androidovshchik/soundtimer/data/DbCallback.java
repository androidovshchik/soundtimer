package rf.androidovshchik.soundtimer.data;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.db.SupportSQLiteOpenHelper;
import android.content.Context;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import timber.log.Timber;

public class DbCallback extends SupportSQLiteOpenHelper.Callback {

    private static final int DATABASE_VERSION = 1;

    public static final String DATABASE_NAME = "current.sqlite";

    private static final String DATABASE_PATH_SUFFIX = "/databases/";

    public DbCallback() {
        super(DATABASE_VERSION);
    }

    @Override
    public void onCreate(SupportSQLiteDatabase db) {}

    @Override
    public void onUpgrade(SupportSQLiteDatabase db, int oldVersion, int newVersion) {}

    public void openDatabase(Context context, String path, boolean delete) {
        File currentDbFile = context.getDatabasePath(DATABASE_NAME);
        if (!currentDbFile.exists() || delete && currentDbFile.delete()) {
            try {
                Timber.d("Coping database from " + path);
                copyDatabase(context, path);
            } catch (IOException e) {
                Timber.e(e);
            }
        } else {
            Timber.d("Missing copy database from " + path);
        }
    }

    private void copyDatabase(Context context, String path) throws IOException {
        FileInputStream fileInputStream = new FileInputStream(path);
        FileOutputStream fileOutputStream = new FileOutputStream(getDatabasePath(context));
        try {
            int length;
            byte[] buffer = new byte[1024];
            while ((length = fileInputStream.read(buffer)) > 0) {
                fileOutputStream.write(buffer, 0, length);
            }
        } finally {
            fileOutputStream.flush();
            fileOutputStream.close();
            fileInputStream.close();
        }
    }

    private String getDatabasePath(Context context) {
        return context.getApplicationInfo().dataDir + DATABASE_PATH_SUFFIX + DATABASE_NAME;
    }
}