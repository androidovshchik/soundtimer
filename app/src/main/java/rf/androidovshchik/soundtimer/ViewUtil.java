package rf.androidovshchik.soundtimer;

import android.content.Context;
import android.content.res.Resources;
import android.util.TypedValue;

public class ViewUtil {

    @SuppressWarnings("unused")
    public static float px2dp(float px) {
        float densityDpi = Resources.getSystem().getDisplayMetrics().densityDpi;
        return px / (densityDpi / 160f);
    }

    @SuppressWarnings("unused")
    public static int dp2px(int dp) {
        float density = Resources.getSystem().getDisplayMetrics().density;
        return Math.round(dp * density);
    }

    @SuppressWarnings("unused")
    public static int sp2px(int sp, Context context) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, sp,
                context.getResources().getDisplayMetrics());
    }
}
