package rf.androidovshchik.soundtimer;

import android.Manifest;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.media.session.MediaControllerCompat;
import android.support.v4.media.session.PlaybackStateCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.nbsp.materialfilepicker.MaterialFilePicker;
import com.nbsp.materialfilepicker.ui.FilePickerActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.android.schedulers.AndroidSchedulers;
import rf.androidovshchik.soundtimer.data.DbManager;
import rf.androidovshchik.soundtimer.data.Prefs;
import rf.androidovshchik.soundtimer.models.rows.Metadata;
import rf.androidovshchik.soundtimer.service.PlayerService;
import timber.log.Timber;

public class MainActivity extends AppCompatActivity {

    public static final int EXTRA_SPACE = ViewUtil.dp2px(8);

    public static final int PERMISSIONS_REQUEST_CODE = 0;
    public static final int FILE_PICKER_REQUEST_CODE = 1;

    @BindView(R.id.container)
    RadioGroup container;

    @BindView(R.id.path)
    EditText path;
    @BindView(R.id.prev)
    ImageButton prev;
    @BindView(R.id.play)
    ImageButton play;
    @BindView(R.id.pause)
    ImageButton pause;
    @BindView(R.id.stop)
    ImageButton stop;
    @BindView(R.id.next)
    ImageButton next;

    PlayerService.PlayerServiceBinder playerServiceBinder;

    MediaControllerCompat mediaController;

    private DbManager dbManager = new DbManager();

    private Prefs prefs;

    private ServiceConnection serviceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            Log.d("MainActivity", "onServiceConnected here");
            playerServiceBinder = (PlayerService.PlayerServiceBinder) service;
            try {
                mediaController = new MediaControllerCompat(getApplicationContext(),
                        playerServiceBinder.getMediaSessionToken());
                mediaController.registerCallback(new MediaControllerCompat.Callback() {
                    @Override
                    public void onPlaybackStateChanged(PlaybackStateCompat state) {
                        if (state == null)
                            return;
                        boolean playing = state.getState() == PlaybackStateCompat.STATE_PLAYING;
                        play.setEnabled(!playing);
                        pause.setEnabled(playing);
                        stop.setEnabled(playing);
                    }
                });
            } catch (RemoteException e) {
                mediaController = null;
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            Log.d("MainActivity", "onServiceDisconnected here");
            playerServiceBinder = null;
            mediaController = null;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        prefs = new Prefs(getApplicationContext());
        if (prefs.has(Prefs.DB_PATH)) {
            container.setOnCheckedChangeListener((RadioGroup radioGroup, int id) -> {
                Timber.d("Checked radio button with id " + id);
            });
            setWaiting();
            String dbPath = prefs.getString(Prefs.DB_PATH);
            path.setText(dbPath);
            dbManager.openDb(getApplicationContext(), dbPath, false);
            loadTables();
        } else {
            setNoDb();
        }
        /*if (!ServiceUtil.isRunning(getApplicationContext(), SoundService.class)) {
            ServiceUtil.startServiceRightWay(getApplicationContext(),
                    ServiceUtil.getIntent(getApplicationContext(), SoundService.class));
        }*/
    }

    private void loadTables() {
        dbManager.onSelectTable("SELECT rowid, * FROM metadata")
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe((Cursor cursor) -> {
                    clearTables();
                    try {
                        while (cursor.moveToNext()) {
                            Metadata metadata = new Metadata();
                            metadata.parseCursor(cursor);
                            addTable(metadata);
                        }
                    } finally {
                        cursor.close();
                    }
                });
    }

    private void addTable(Metadata metadata) {
        RadioButton button = new RadioButton(getApplicationContext());
        button.setId(View.generateViewId());
        button.setLayoutParams(new RadioGroup.LayoutParams(
                RadioGroup.LayoutParams.MATCH_PARENT,
                RadioGroup.LayoutParams.WRAP_CONTENT
        ));
        ((ViewGroup.MarginLayoutParams) button.getLayoutParams())
                .setMargins(0, container.getChildCount() <= 0 ? EXTRA_SPACE * 2 : 0, 0, EXTRA_SPACE);
        button.setPadding(EXTRA_SPACE, 0, 0, 0);
        button.setText(metadata.title);
        button.setTextColor(Color.parseColor("#dd000000"));
        button.setTextSize(16);
        container.addView(button);
    }

    private void addText(String text) {
        clearTables();
        TextView textView = new TextView(getApplicationContext());
        textView.setPadding(0, EXTRA_SPACE * 2, 0, EXTRA_SPACE);
        textView.setText(text);
        textView.setTextColor(Color.parseColor("#dd000000"));
        textView.setTextSize(16);
        container.addView(textView);
    }

    private void clearTables() {
        container.removeAllViews();
    }

    private void setNoDb() {
        addText("Не выбрана база данных");
    }

    private void setNoTables() {
        addText("Отсутствуют графики в текущей базе данных");
    }

    private void setWaiting() {
        addText("Пожалуйста, подождите...");
    }

    @OnClick(R.id.choose)
    void onChoose() {
        checkPermissionsAndOpenFilePicker();
    }

    @OnClick(R.id.prev)
    void onPrevClick() {
        if (mediaController != null) {
            //mediaController.getTransportControls().skipToPrevious();
        }
    }

    @OnClick(R.id.play)
    void onPlayClick() {
        if (mediaController != null) {
            //mediaController.getTransportControls().play();
        }
    }

    @OnClick(R.id.pause)
    void onPauseClick() {
        if (mediaController != null) {
            //mediaController.getTransportControls().pause();
        }
    }

    @OnClick(R.id.stop)
    void onStopClick() {
        if (mediaController != null) {
            //mediaController.getTransportControls().stop();
        }
    }

    @OnClick(R.id.next)
    void onNextClick() {
        if (mediaController != null) {
            //mediaController.getTransportControls().skipToNext();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        bindService(new Intent(getApplicationContext(), PlayerService.class),
                serviceConnection, BIND_AUTO_CREATE);
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d("MainActivity", "unbindService here");
        unbindService(serviceConnection);
    }

    private void checkPermissionsAndOpenFilePicker() {
        String permission = Manifest.permission.READ_EXTERNAL_STORAGE;
        if (ContextCompat.checkSelfPermission(getApplicationContext(), permission) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, permission)) {
                showError("Запрещен доступ на чтение внутреннего хранилища");
            } else {
                ActivityCompat.requestPermissions(this, new String[]{permission}, PERMISSIONS_REQUEST_CODE);
            }
        } else {
            openSqlitePicker();
        }
    }

    private void showError(@NonNull String message) {
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
    }

    private void openSqlitePicker() {
        new MaterialFilePicker()
                .withActivity(this)
                .withRequestCode(FILE_PICKER_REQUEST_CODE)
                .withHiddenFiles(false)
                .start();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSIONS_REQUEST_CODE: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    openSqlitePicker();
                } else {
                    showError("Нет разрешения на чтение внутреннего хранилища");
                }
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == FILE_PICKER_REQUEST_CODE && resultCode == RESULT_OK) {
            String filePath = "" + data.getStringExtra(FilePickerActivity.RESULT_FILE_PATH);
            if (!filePath.matches(".*\\.(db|sdb|sqlite|db3|s3db|sqlite3|sl3|db2|s2db|sqlite2|sl2)$")) {
                showError("Пожалуйста, выберите файл соотвествующий формату Sqlite");
                setNoDb();
                prefs.remove(Prefs.DB_PATH);
                path.setText("");
            } else {
                setWaiting();
                prefs.putString(Prefs.DB_PATH, filePath);
                dbManager.openDb(getApplicationContext(), filePath, true);
                path.setText(filePath);
            }
            path.clearFocus();
        }
    }
}
